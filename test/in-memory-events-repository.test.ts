import {eventSource} from '../src'
import {Event, EventSourceConfiguration} from '@othree.io/chisel'

describe('InMemoryEventSource', () => {
    const configuration: EventSourceConfiguration = {
        SnapshotEventType: 'SNAPSHOT',
        SnapshotFrequency: 100
    }
    describe('getEvents', () => {
        it('should return an empty array if the contextId does not have any events', async () => {
            const {getEvents} = eventSource.InMemoryEventSource(configuration)

            const maybeEvents = await getEvents('notFound')

            expect(maybeEvents.isPresent).toBeTruthy()
            expect(maybeEvents.get()).toStrictEqual([])
        })

        it('should return the persisted events', async () => {
            const {getEvents, persist} = eventSource.InMemoryEventSource(configuration)

            const eventOne: Event = {
                eventId: '001',
                eventDate: 9000,
                contextId: '1337',
                type: 'ValueAdded',
                body: { value: 100 }
            }

            await persist(eventOne)

            const eventTwo: Event = {
                eventId: '002',
                eventDate: 9001,
                contextId: '1337',
                type: 'ValueAdded',
                body: { value: 200 }
            }

            await persist(eventTwo)

            const maybeEvents = await getEvents('1337')

            expect(maybeEvents.isPresent).toBeTruthy()
            expect(maybeEvents.get()).toStrictEqual([
                eventTwo,
                eventOne
            ])
        })
    })
    describe('persist', () => {
        it('should persist the specified event', async () => {
            const {persist} = eventSource.InMemoryEventSource(configuration)

            const event: Event = {
                eventId: '001',
                eventDate: 9000,
                contextId: '1337',
                type: 'ValueAdded',
                body: { value: 100 }
            }

            const maybeEvent = await persist(event)

            expect(maybeEvent.isPresent).toBeTruthy()
            expect(maybeEvent.get()).toStrictEqual(event)
        })

        it('should persist the specified snapshot event and clean previously recorded ones', async () => {
            const {persist, getEvents} = eventSource.InMemoryEventSource(configuration)

            const eventOne: Event = {
                eventId: '001',
                eventDate: 9000,
                contextId: '1337',
                type: 'ValueAdded',
                body: { value: 100 }
            }

            await persist(eventOne)

            const eventTwo: Event = {
                eventId: '002',
                eventDate: 9001,
                contextId: '1337',
                type: 'ValueAdded',
                body: { value: 200 }
            }

            await persist(eventTwo)

            const snapshot: Event = {
                eventId: '003',
                eventDate: 9003,
                contextId: '1337',
                type: configuration.SnapshotEventType,
                snapshot: { value: 300 },
                body: {}
            }

            await persist(snapshot)

            const maybeEvents = await getEvents('1337')

            expect(maybeEvents.isPresent).toBeTruthy()
            expect(maybeEvents.get()).toStrictEqual([
                snapshot
            ])
        })
    })
})