import {EventNotifierConfiguration, actor} from '../src'
import {Event} from '@othree.io/chisel'

describe('WebWorkerEventNotifier', () => {
    interface ValueContainer {
        readonly value: number
    }

    const configuration: EventNotifierConfiguration = {
        boundedContext: 'operation'
    }

    describe('publish', () => {
        it('should publish the event and state', async () => {
            const worker : Partial<Worker> = {
                postMessage: jest.fn()
            }

            const publishEvent = actor.publishEvent(worker as any)<ValueContainer>(configuration)

            const event: Event = {
                type: 'ValueAdded',
                eventId: '001',
                eventDate: 9000,
                contextId: '1337',
                body: { value: 100 }
            }

            const state: ValueContainer = {
                value: 100
            }

            const maybeSuccess = await publishEvent(event, state)

            expect(maybeSuccess.isPresent).toBeTruthy()
            expect(maybeSuccess.get()).toBeTruthy()

            expect(worker.postMessage).toBeCalledTimes(1)
            expect(worker.postMessage).toBeCalledWith({
                boundedContext: configuration.boundedContext,
                event,
                state
            })
        })
    })
})