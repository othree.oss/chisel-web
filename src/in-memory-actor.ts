import {TryAsync} from '@othree.io/optional'
import {actor, Event} from '@othree.io/chisel'
import {EventNotifierConfiguration} from './types'

export const publishEvent = (worker: Worker) =>
    <AggregateRoot>(configuration: EventNotifierConfiguration): actor.PublishEvent<AggregateRoot> =>
        async (event: Event, state: AggregateRoot) => {
            return TryAsync(async () => {
                worker.postMessage({
                    boundedContext: configuration.boundedContext,
                    event: event,
                    state: state
                })

                return true
            })
        }
