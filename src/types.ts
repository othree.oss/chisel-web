export interface EventNotifierConfiguration {
    readonly boundedContext: string
}