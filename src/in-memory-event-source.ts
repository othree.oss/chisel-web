import {eventSource, Event, EventSourceConfiguration} from '@othree.io/chisel'
import {Optional} from '@othree.io/optional'

type EventsType = { [key: string]: Array<Event> }

export function InMemoryEventSource(configuration: EventSourceConfiguration) {
    let events: EventsType = {}

    const persist: eventSource.PersistEvent = async (event: Event): Promise<Optional<Event>> => {
        const contextEvents = Optional(events[event.contextId])
            .map(_ => {
                if (event.type === configuration.SnapshotEventType) {
                    return [event]
                }
                return [..._, event]
            })
            .orElse([event])

        events[event.contextId] = contextEvents

        return Optional(event)
    }

    const getEvents = async (contextId: string): Promise<Optional<Array<Event>>> => {
        return Optional(events[contextId])
            .map(events => [...events].reverse())
            .orMap(() => [])
    }

    return Object.freeze({
        persist,
        getEvents
    })
}
